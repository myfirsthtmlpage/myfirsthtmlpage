<?php 
     
        // On va vérifier les variables
        require('fonctions.php'); // On réclame le fichier
        $LastName=$_POST['inputLastName'];
        $FirstName=$_POST['inputFirstName'];
        $Type=$_POST['inputType'];
        $Nom=$_POST['inputName'];
        $DateArrive=$_POST['inputDateArrive'];
        $DateDepart=$_POST['inputDateDepart'];
        $Nationnalite=$_POST['inputNationnalite'];
        $checkPDF=false;
        $checkImage=false;
        if($_FILES["inputPDF"]["type"] == "application/pdf"){
          if($_FILES["inputPDF"]["error"] > 0)
          {
            echo "Return Code:".$_FILES["inputPDF"]["error"] ."<br/>";
          }else{
            echo "Upload: " . $_FILES["inputPDF"]["name"] . "<br />";
            echo "Type: " . $_FILES["inputPDF"]["type"] . "<br />";
            echo "Size: " . ($_FILES["inputPDF"]["size"] / 1024) . " Kb<br />";
            echo "Temp inputImage: " . $_FILES["inputPDF"]["tmp_name"] . "<br />";
            if (file_exists("upload/" . $_FILES["inputImage"]["name"]))
              {
              echo $_FILES["inputPDF"]["name"] . " already exists. ";
              }else{
                $Caracteristique=$_FILES["inputPDF"]["name"];
                move_uploaded_file($_FILES["inputPDF"]["tmp_name"], "upload/" . $Caracteristique);
                echo "path: " . "upload/" . $_FILES["inputPDF"]["name"];
                $checkPDF=true;
              }
          }
        }else{
          echo "Invalid inputPDF";
        }

        if ((($_FILES["inputImage"]["type"] == "image/png")|| ($_FILES["inputImage"]["type"] == "image/jpeg")|| ($_FILES["inputImage"]["type"] == "image/jpg"))
            || ($_FILES["inputImage"]["type"] == "image/pjpeg")||($_FILES["inputImage"]["type"] == "image/gif"))
          {
          if ($_FILES["inputImage"]["error"] > 0)
            {
            echo "Return Code: " . $_FILES["inputImage"]["error"] . "<br />";
            }
          else
            {
            echo "Upload: " . $_FILES["inputImage"]["name"] . "<br />";
            echo "Type: " . $_FILES["inputImage"]["type"] . "<br />";
            echo "Size: " . ($_FILES["inputImage"]["size"] / 1024) . " Kb<br />";
            echo "Temp inputImage: " . $_FILES["inputImage"]["tmp_name"] . "<br />";
        
            if (file_exists("upload/" . $_FILES["inputImage"]["name"]))
              {
              echo $_FILES["inputImage"]["name"] . " already exists. ";
              }
            else
              {
                //$Image = addslashes(file_get_contents($_FILES['inputImage']['tmp_name']));
                
                $Image=$_FILES["inputImage"]["name"];
                move_uploaded_file($_FILES["inputImage"]["tmp_name"], "upload/" . $Image);
                echo "path: " . "upload/" . $_FILES["inputImage"]["name"];
                $checkImage=true;
           
              }
            }
          }
        else
          {
          echo "Invalid inputImage";
          }
          //checkImage,checkPDF,checkOwner and checkDate are true, create boat
          //if not delete image or pdf
          $query=Armada_FindOwner($LastName,$FirstName);
          if($query==null){
            $checkOwner=false;
            echo "<script>alert('Owner does not exist');location.href='boat.php'</script>";
          }else{
            $idUser= $query['id'];
            $checkOwner=true;
          }

          if(Armada_CheckDate($DateArrive,$DateDepart)===FALSE){
            $checkDate=false;
            echo "<script>alert('Date is wrong');location.href='boat.php'</script>";
          }else{
            $checkDate=true;
          }
          if(($checkImage==true)&&($checkPDF==true)&&($checkOwner==true)&&($checkDate==true)){
            Armada_SetBoat($idUser,$Type,$Nom,$DateArrive,$DateDepart,$Nationnalite,$Image,$Caracteristique);
            echo "<script>alert('create successfully');location.href='utilisateur.php';</script>";
          }else{
            $pathBateau="upload/" . $Image;
            $pathPDF="upload/" . $Caracteristique;
            if(file_exists($pathBateau)){
               chmod($pathBateau,0777);
                if(unlink($pathBateau)){
                   echo "success";
                   
                }else{
                    echo "fail";
                }
            }
            if(file_exists($pathPDF)){
               chmod($pathPDF,0777);
                if(unlink($pathPDF)){
                   echo "success";
                   
                }else{
                    echo "fail";
                }
            }
          }
?>
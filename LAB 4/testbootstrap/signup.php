<?php 
include_once 'fonctions.php'
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Inscription of our site Armada,it can be linked to login.php and create an account by
                                          filling out the form">
    <meta name="author" content="Lijun">
    

    <title>Sign up Armada</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--link href="styles/login.css" rel="stylesheet"-->
    <link href="styles/login.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!--link href="signin.css" rel="stylesheet"-->
  </head>

  <body  >
    <div class = "container">
      <div class="row">
        <div class="col-sm-7">
    
          <form class="form" method="post" action="check.php">
        

           <h1 class="h3 mb-3 font-weight-normal">Please sign up</h1>
           <div class="form-row ">
              <div class="col-md-6 mb-3">
               <label for="inputFirstName" >First Name</label>
               <input type="text" name="inputFirstName" class="form-control" placeholder="Prénom" required autofocus>
              </div>
              <div class="col-md-6 mb-3">
                 <label for="inputLastName" >Last Name</label>  
                 <input type="text" name="inputLastName"  class="form-control" placeholder="Nom" required>
              </div> 
          </div> 
          <div class="form-row">
            <div class="col-md-12 mb-3">
               <label for="inputEmail" >Email</label>
               <input type="email" name="inputEmail"  class="form-control" placeholder="E-mail" required>
             </div>
          </div>
          <div class="form-row">
              <div class="col-md-6 mb-3">
                 <label for="inputPassword" >Password</label>
                 <input type="password" name="inputPassword"  class="form-control" placeholder="Password" required>
               </div>
              <div class="col-md-6 mb-3">
                 <label for="inputPassword" >Check Password</label>
                 <input type="password" name="checkPassword"  class="form-control" placeholder="Check Password" required>
              </div>
            </div>
        
              <button class="btn btn-lg btn-primary " type="submit" name= "signup">Sign up</button>
              <a href="login.php">déja avoir un compte?</a>
          

             <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
      
          </form>
        </div>
      </div>
    </div>
   

    
    

   

    
  </body>
</html>
<?php
include 'fonctions.php';
$masque=$_GET['var'];
if(!isset($_COOKIE['ArmadaLogin']))
{
    echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
    

        <title>Tableau des bateaux</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/utilisateur.css" rel="stylesheet">
    
    
    </head>
    <body>

        
        <a href="index.php"><div class="jumbotron"></div></a>
        <div class="table-responsive">
        <div class="container">
            <div class="row">
                <div class="col-sm-8"> 
                    <div class="table-reposnive">
                    <table class="table table-bordered table-striped">
                        <caption>Tableau des bâteaux</caption>
                        <thead>
                            <tr>
                                <td colspan="2"></td>
                                <th scope="col">Nom du bâteau</th>
                                <th scope="col">Nationnalité</th>
                                <th scope="col">Type du bâteau</th>
                                <th scope="col">Date d'arrive</th>
                                <th scope="col">Date de départ</th>
                                <th scope="col">Information détaillé</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                            if($masque==0){ 
                                $BoatArray=Armada_GetBoat();
                                for($i=0;$i<count($BoatArray);$i++){
                                    echo '<tr>';
                                    echo '<th colspan="2" scope="row"><img class="boat" widith="100" height="100" src="upload/'.$BoatArray[$i]['imageBateau'].'"/></th>';
                                    echo '<th>'.$BoatArray[$i]['nomBateau'].'</th>';
                                    echo '<th>'.$BoatArray[$i]['nationnalite'].'</th>';
                                    echo '<th>'.$BoatArray[$i]['typeBateau'].'</th>';
                                    echo '<th>'.$BoatArray[$i]['dateArrive'].'</th>';
                                    echo '<th>'.$BoatArray[$i]['dateDepart'].'</th>';
                                    echo '<th><a href="upload/'.$BoatArray[$i]['caracteristique'].'">Télecharger</a></th>';
                                    echo '</tr>';
                                }
                            }else if($masque==1){
                            
                                $boatName=$_POST['search'];
                                $boat=Armada_GetBoat4($boatName);
                                if($boat==null){
                                    echo "bâteau n'existe pas.";
                                }
                                echo '<tr>';
                                echo '<th colspan="2" scope="row"><img class="boat" widith="100" height="100" src="upload/'.$boat['imageBateau'].'"/></th>';
                                echo '<th>'.$boat['nomBateau'].'</th>';
                                echo '<th>'.$boat['nationnalite'].'</th>';
                                echo '<th>'.$boat['typeBateau'].'</th>';
                                echo '<th>'.$boat['dateArrive'].'</th>';
                                echo '<th>'.$boat['dateDepart'].'</th>';
                                echo '<th><a href="upload/'.$boat['caracteristique'].'">Télecharger</a></th>';
                                echo '</tr>';
                            }
                            ?>
                            
                        </tbody>
                    </table>
                    </div>
                </div>
                
            </div>
        </div>
        </div>
        <hr>
        <?php
            include 'footer.html'; 
        ?>

    </body>
    

   
</html>
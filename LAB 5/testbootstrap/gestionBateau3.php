<?php
include 'fonctions.php';
$idBateau=$_GET['var'];
if(!isset($_COOKIE['ArmadaLogin']))
{
  echo "<script>alert('You need to login first');location.href='login.php'</script>";
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Gestion du bâteau</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
 
  </head>

  <body  >
    
    <div class="container">
        <div class ="row">
          <div class="col-sm-6">
            
            <?php $boat=Armada_GetBoat3($idBateau); 
                  $user=Armada_GetName($boat['idUser']);
            ?>
            <form class ="form-horizontal"role="form" method="post" action="checkGestionBateau3.php?var=<?php echo $idBateau;?>" enctype="multipart/form-data">
                <div class="form-group">
                     <h1 class="h3 mb-3 font-weight-normal">Gestion du bâteau</h1>
                     <label for="inputPDF" >Information détaillé du bateau</label>
                     <input type="file" name="inputPDF" accept= "application/pdf" class="form-control"  required autofocus>
                    
                </div>

                <button class="btn btn-lg btn-primary " type="submit" name= "modifier">Modifier</button>
                
            
            </form>
          </div>
        </div>
    </div>
    
  
</body>
</html>
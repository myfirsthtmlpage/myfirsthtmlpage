<?php
include_once 'fonctions.php';
if(!isset($_COOKIE['ArmadaLogin']))
{
    echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
    

        <title>Page d'utilisateur</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/utilisateur.css" rel="stylesheet">
        
    
    
    </head>
    <body>
        <a href="index.php"><div class="jumbotron"></div></a>
        <main>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h3>Page d'utilisateur</h3> 
                    <?php
                    $user = Armada_GetUtilisateur($_COOKIE['ArmadaLogin']);
                    $statut=Armada_GetStatut($user['idStatut']);
                    echo "<p> Nom:".$user['nom']."</p>";
                    echo "<p> Préom:".$user['prenom']."</p>";
                    echo "<p> Email:".$user['email']."</p>";
                    echo "<p> Fonction:".$statut['fonction']."</p>";
                    if(!empty($_COOKIE['lastVisit'])){
                        echo '<p>Your last login time：'.$_COOKIE['lastVisit'].'</p>';
                        //refresh
                        setcookie("lastVisit",date("Y-m-d H:i:s"),time()+24*3600*30); 
                    }else{
                        //first time login
                        echo "<p>you first login time：</p>";
                        setcookie("lastVisit",date("Y-m-d H:i:s"),time()+24*3600*30);
                    }
                    if($user['idStatut']!=1){
                    ?>
                         <a href="boat.php">créer un bateau</a>
                    <?php } ?>

                            <form action="utilisateur.php" method="post">
                                <input type="submit" name="deconnect" value="Deconnecter" />
                            </form>

                            <?php
                                if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['deconnect']))
                                {
                                    setcookie('ArmadaLogin',NULL);
                                    header('Location: index.php');
                                }
                            ?>
                </div><!--/col-3-->
                                <br>
                
                <div class="col-sm-9">
                       
                         <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Navires</a></li>
                                <?php 
                                    if($user['idStatut']==2){
                                        echo ' <li><a href="#bateau" data-toggle="tab" >Gestion du bateau </a></li>';  
                                    }else if($user['idStatut']==3){
                                        echo ' <li><a href="#bateau" data-toggle="tab" >Gestion du bateau </a></li>';
                                        echo '<li><a href="#personne"data-toggle="tab" >Gestion de personne</a></li>';
                                    }
                                ?>                                
                        </ul>
                                 
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">
                                    
                                    <div id="carousel" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                         <?php 
                                            $ImageArray=Armada_GetBoat();
                                             for($index=0;$index<count($ImageArray);$index++){
                                              if($index==0){
                                                  echo ' <li data-target="#carousel" data-slide-to="'.$index.'" class="active"></li>';
                                               }else{
                                                 echo ' <li data-target="#carousel" data-slide-to="'.$index.'"></li>';
                                              }
                                            }
                                        ?>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            <?php 
                                                for($i=0;$i<count($ImageArray);$i++){
                                                    if($i==0){
                                                    echo '<div class="item active">
                                                             <img class="img-responsive" width="200" height="200" src="upload/'.$ImageArray[$i]['imageBateau'].'"/>
                                                             <div class="carousel-caption"></div>
                                                        </div>';
                                                    }else{
                                                    echo '<div class="item">
                                                                <img class="img-responsive" width="200" height="200" src="upload/'.$ImageArray[$i]['imageBateau'].'"/> 
                                                                <div class="carousel-caption"></div>
                                                        </div>';
                                                        }
                                                }
                               
                                             ?>
                                        </div><!-- /carousel-inner -->
                                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                         </a>
                                    </div><!--/carousel -->
                                            
                                </div><!-- /home -->
                                <div class="tab-pane" id="bateau">
                                           <div class="table-responsive">     
                                                <table class="table table-bordered table-striped">
                                                    <?php
                                                    echo '<caption>Les bâteaux de '.$user['nom'].' &nbsp'.$user['prenom'].' </caption>';
                                                    ?>
                                                    <thead>
                                                        <tr>
                                                            <td></td>
                                                            <th scope="col">Nom du bâteau</th>
                                                            <th scope="col">Information détaillé</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            if($user['idStatut']==2){
                                                                $BoatArray=Armada_GetBoat2($user['email']);
                                                            }else if($user['idStatut']==3){
                                                                 $BoatArray=Armada_GetBoat();
                                                            }
                                                            for($i=0;$i<count($BoatArray);$i++){
                                                        ?>
                                                  
                                                        <tr>
                                                        <td scope="row"><a href="gestionBateau2.php?var=<?php echo $BoatArray[$i]['idBateau']; ?>"><img class="boat" widith="100" height="100" src="upload/<?php echo $BoatArray[$i]['imageBateau']; ?>"/></td>                                                  
                                                        <td><a href="gestionBateau.php?var=<?php echo $BoatArray[$i]['idBateau']; ?>"><?php echo $BoatArray[$i]['nomBateau']; ?></a></td>
                                                        <td><a href="gestionBateau3.php?var=<?php echo $BoatArray[$i]['idBateau']; ?>"><?php echo $BoatArray[$i]['caracteristique']; ?></a></td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                </div><!-- /bateau -->
                                
                                <div class="tab-pane" id="personne">
                                              <form role="form" method="post" action="checkUtilisateur.php" enctype="multipart/form-data">
                                                  <div class="form-group">
                                                      <label for="name">Select une personne et modifier role</label>
                                                      <select multiple class="form-control" name="optionName">
                                                          <?php 
                                                             $userArray=Armada_GetAllUtilisateur();
                                                             for($i=0;$i<count($userArray);$i++){
                                                                 echo '<option  value="'.$userArray[$i]['id'].'">'.$userArray[$i]['prenom'].'&nbsp'.$userArray[$i]['nom'].'</option>';
                                                             }
                                                          ?>
                                                      </select>
                                                      
                                                          <?php

                                                          
                                                          $statutArray=Armada_GetAllStatut();                     
                                                          for($i=0;$i<count($statutArray);$i++){
                                                              if($statutArray[$i]['statut_id']==1){
                                                              echo '<div class="radio">
                                                                        <label>
                                                                            <input type="radio" name="optionsRadios" value="'.$statutArray[$i]['statut_id'].'" checked>'.$statutArray[$i]['fonction'].'
                                                                        </label>
                                                                    </div>';
                                                              }else{
                                                              echo '<div class="radio">
                                                                        <label>
                                                                             <input type="radio" name="optionsRadios" value="'.$statutArray[$i]['statut_id'].'">'.$statutArray[$i]['fonction'].'
                                                                        </label>
                                                                    </div>';
                                                                    
                                                              }
                                                          }
                                                          ?>
                                                  </div><!-- /form-group -->
                                                  <button class="btn btn-lg btn-primary btn-block" type="submit" name= "yes">Yes</button>
                                              </form>  
                                </div><!-- /personne -->
                                                
                    </div><!--tab-content-->
                    
                </div><!--/col-9-->
                
            </div><!--/row-->
        </div><!--/container-->
        </main>
        <hr>
        <?php
            include 'footer.html'; 
        ?>










        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        

    </body>
    
    
 
</html>
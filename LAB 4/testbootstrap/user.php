<?php
include 'fonctions.php';
if(!isset($_COOKIE['ArmadaLogin']))
{
    echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Etape 4</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Etape 4</h1>
    <p>Page d'accuil, vous êtes connecté !</p>
    <p>
        <?php
            $user = Armada_GetUtilisateur($_COOKIE['ArmadaLogin']);
            echo 'Bonjour '.$user['prenom'].' '.$user['nom'];
            
            $boat=Armada_GetBoat();
            //echo '<img src="data:image/jpeg;base64,'.base64_encode( $boat['imageBateau'] ).'"/>';
        ?>
        <a href="boat.php">créer un bateau</a>

            <form action="user.php" method="post">
                <input type="submit" name="deconnect" value="Deconnecter" />
            </form>
            
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['deconnect']))
                {
                    setcookie('ArmadaLogin',NULL);
                    header('Location: index.php');
                }
                
            ?>
	</p>
</body>

</html>

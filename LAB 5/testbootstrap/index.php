<?php 
include_once 'fonctions.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>ARMADA</title>
    <link href="styles/homepage.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   
  </head>

  <body>
    <main role="main">
      <!-- Main jumbotron  -->
      <header><div class="jumbotron"></div></header>

      <div class="container">
        <div class="row">
            <div class="col-md-10">
                <header>
                    <h1>
                      Welcome to ARMADA!
                    </h1>
                </header>
            </div>
            <!--  a form to searh the boats          -->
            
            <div class="col-md-2">
                <?php
                  if(isset($_COOKIE['ArmadaLogin'])){
                    $user = Armada_GetUtilisateur($_COOKIE['ArmadaLogin']);
                    echo 'Bonjour <a href="utilisateur.php" title="espace personnel">'.$user['prenom'].' '.$user['nom'].'</a>';
                    echo "<form action='index.php' method='post'>";
                    echo "<input type='submit' name='deconnect' value='Deconnecter' /></form>";

                    if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['deconnect']))
                    {
                    setcookie('ArmadaLogin',NULL);
                    header('Location: index.php');
                    }
                  
                  }else {
                ?>
                <img src="images/connecter.png" alt="sailor"><a href="login.php"> se connecter</a>
                <?php
                  }
                  
                ?>
                <form class="form-search" method="post" action="boatTable.php?var=1" enctype="application/x-www-form-urlencoded">
                    <input type="text" id="search" name="search" class="input-medium search-query typeahead" placeholder="Nom du bâteau">
                    
                    <button type="submit" class="btn">search</button>
                </form>
            </div>
        </div>
      </div> 
      
      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-sm-10">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab">L'ARMADA</a></li>
              <li><a href="#programme" data-toggle="tab" >PROGRAMME</a></li>
              <li><a href="#navires"data-toggle="tab" >NAVIRES</a></li>
            </ul>
         
              <div class="tab-content">
                <div class="tab-pane active" id="home">
                  <p>L'Armada 2019 est organisée par l'association de " l'Armada de la liberté ". 
                     Cette manifestation peut une nouvelle fois voir le jour grâce à l'action
                     d'une armada de bénévoles fédérés autour de leur président, Patrick Herr.
                  </p>
                  <p>L'Armada 2019 marquera la septième édition de cette manifestation qui rassemble
                     les plus beaux bateaux du monde sur les quais de Rouen. 
                     Retour sur 30 ans d'histoire de l'Armada...
                     
                  </p>
                </div>
                <div class="tab-pane" id="programme">
                    <p>
                      Programme prévisionnel de l'Armada 2019:
                      <ol>
                        <li >Jeudi 6 Juin et vendredi 7 juin : Arrivée des bateaux et levée du Pont Gustave Flaubert entre 5h et 6 h et de 21h à minuit</li>
                        <li>Samedi 8 juin : Inauguration officielle et dîner des Capitaines</li>
                        <li>Dimanche 9 juin : Messe des marins</li>
                        <li>Mercredi 12 juin : Défilé des équipages</li>
                        <li>Vendredi 14 juin : Congrès des Villes Marraines des Forces Armées</li>
                        <li>Samedi 15 juin : Grand footing des marins</li>
                        <li>Dimanche 16 juin : Grande Parade à partir de 11h avec survol de la Patrouille de France</li>
                      </ol>
                      Les animations envahiront également le centre-ville de Rouen et la vallée de Seine.  Plusieurs expositions sont déjà annoncées pendant la période de l’Armada 2019.  
                    </p>
                    
                </div>

                <div class="tab-pane" id="navires">
                  <br>
                  <div class="table-responsive">
                  <table class="table">
                    
                    <?php 
                      $ImageArray=Armada_GetBoat();
                      for($index=0;$index<count($ImageArray);$index=$index+3){
                    ?>
                    <tr >                    
                      <td >
                          <figure>
                              
                             <?php echo '<a href="boatTable.php?var=0"><img class="boat" widith="200" height="200" title="plus d\'information?" src="upload/'.$ImageArray[$index]['imageBateau'].'"/>'; ?>
                              <figcaption><?php echo $ImageArray[$index]['nomBateau'];?></figcaption></a>
                            </figure>
                      </td>
                      <?php
                        if($index+1===count($ImageArray)){
                          break;
                        }
                      ?>
                      <td>
                        <figure>
                           <?php echo '<a href="boatTable.php?var=0"><img class="boat" widith="200" height="200" src="upload/'.$ImageArray[$index+1]['imageBateau'].'"/>'; ?>
                           <figcaption><?php echo $ImageArray[$index+1]['nomBateau'];?></figcaption></a>
                        </figure>
                      </td>
                      <?php
                        if($index+2===count($ImageArray)){
                          break;
                        }
                      ?>
                      <td >
                        <figure>
                           <?php echo '<a href="boatTable.php?var=0"><img class="boat" widith="200" height="200" src="upload/'.$ImageArray[$index+2]['imageBateau'].'"/>'; ?>
                           <figcaption><?php echo $ImageArray[$index+2]['nomBateau'];?></figcaption></a>
                        </figure>
                      </td>
                    </tr>
                    <br/>
                    <?php
                      }
                    ?>
                    
                  </table>
                    </div>

                </div><!-- /navires -->
                
              </div><!-- /tab-content -->
          </div>

          <div class="col-sm-2">
            <br><br>
              <img src="images/carte_rouen.jpg" alt="carte de Rouen">
          </div>
      </div>
    </div> <!-- /container -->
      
  </main>
  <hr>
     <?php
     include 'footer.html'; 
     ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <!--script src="scripts/main.js"></script-->
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script-->
  </body>
</html>
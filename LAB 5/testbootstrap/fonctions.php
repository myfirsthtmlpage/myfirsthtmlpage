<?php
    //Connect the database
    function Armada_Connection(){
            $servername = "localhost";
            $username = "root";
            $password = "321117";
            $datebase = "armada";
            $con = new mysqli($servername, $username, $password,$datebase);
        if (!$con) {
            echo "Connect Error: " . mysqli_connect_error();
            exit();
        }
        return $con;
    
    }
    //Get the user by checking the email
    function Armada_GetUtilisateur($login)
    {
        $con = Armada_Connection();
        $sql = 'SELECT * FROM user where email = \''.$login.'\''; 
        $query  = mysqli_query($con, $sql); 
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }

    function Armada_GetAllUtilisateur()
    {
        $con = Armada_Connection();
        $sql = "SELECT * FROM user";
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        $userArray=array();
        if ($rowCount > 0) {
            while($row = mysqli_fetch_array($query)){
                $userArray[]=$row;
            }
        }
        return $userArray;
    }

    function Armada_GetAllStatut()
    {
        $con = Armada_Connection();
        $sql = "SELECT * FROM statut";
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        $statutArray=array();
        if ($rowCount > 0) {
            while($row = mysqli_fetch_array($query)){
                $statutArray[]=$row;
            }
        }
        return $statutArray;
    }
    function Armada_GetStatut($idStatut)
    {
        $con = Armada_Connection();
        $sql = 'SELECT * FROM statut where statut_id = \''.$idStatut.'\''; 
        $query  = mysqli_query($con, $sql); 
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }
    
    //Sign in
    function Armada_ConnectUtilisateur($login, $password)
    {
        $con = Armada_Connection();
        $md5Password=md5($password);
        //$sql = 'SELECT * FROM user where email = \''.$login.'\' and password1 = \''.$md5Password.'\''; 
        $sql = "SELECT * FROM user where email = '$login' and password1 = '$md5Password' ";
        $query  = mysqli_query($con, $sql); 
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        if ($rowCount == 1) {
            return true;
        }
    
        return false;
    }
    //Set user in the database.
    function Armada_SetUtilisateur($LastName,$FirstName,$Email,$Password)
    {
        $con = Armada_Connection();
        $md5Password=md5($Password);
        $sql="INSERT INTO user (nom,prenom,email,password1) VALUES ('$LastName','$FirstName','$Email','$md5Password')";
        //$sql= 'INSERT INTO user (nom,prenom,email,password1) VALUES (\''.$LastName.'\',\'
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query); 
    }
    //Check the email in the database,if this email already does not exist in database return true ,else return true.
    function Armada_CheckEmail($Email)
     {
        $con = Armada_Connection();
        $sql = 'SELECT * FROM user where email = \''.$Email.'\''; 
        $query  = mysqli_query($con, $sql); 
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return true;
        }
        return false;
     }

     function Armada_FindOwner($LastName,$FirstName)
     {
        $con = Armada_Connection();
        
        $sql = 'SELECT * FROM user where nom = \''.$LastName.'\' and prenom = \''.$FirstName.'\'';
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);
        
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return null;
            
        }
        return mysqli_fetch_array($query);
     }
     //Create boat in the database
     
     function Armada_SetBoat($idUser,$Type,$Nom,$DateArrive,$DateDepart,$Nationnalite,$Image,$Caracteristique)
     {
       
        $con = Armada_Connection();
        
        $sql = "INSERT INTO boat (idUser,nomBateau,nationnalite,typeBateau,caracteristique,dateArrive,dateDepart,imageBateau) VALUES ('$idUser','$Nom','$Nationnalite','$Type','$Caracteristique','$DateArrive','$DateDepart','$Image')";
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);

        
        mysqli_close($con);
        
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }

    function Armada_GetBoat()
    {
        $con = Armada_Connection();
        $sql = "SELECT * FROM boat";
        $query  = mysqli_query($con, $sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        $imageArray=array();
        if ($rowCount > 0) {
            while($row = mysqli_fetch_array($query)){
                $imageArray[]=$row;
            }
        }
        return $imageArray;
    }
    
    function Armada_GetBoat2($email){
        $con = Armada_Connection();
        $sql = 'SELECT * FROM boat inner join user on boat.idUser=user.id where email = \''.$email.'\''; 
        //$sql = "SELECT * FROM boat inner join user on boat.idUser=user.id where email='$email'";
        $query=mysqli_query($con,$sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        $boatArray=array();
        if ($rowCount > 0) {
            while($row = mysqli_fetch_array($query)){
                $boatArray[]=$row;
            }
        }
        return $boatArray;
    }

    function Armada_GetBoat3($idBateau){
        $con = Armada_Connection();
        $sql = "SELECT * FROM boat WHERE idBateau = '$idBateau'";
        $query = mysqli_query($con,$sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }

    function Armada_GetBoat4($nomBateau){
        $con = Armada_Connection();
        $sql = "SELECT * FROM boat WHERE nomBateau = '$nomBateau'";
        $query = mysqli_query($con,$sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }

    function Armada_UpdateBoat($nom,$nationnalite,$type,$dateArrive,$dateDepart,$idUser,$idBateau){
        $con = Armada_Connection();
        $sql = "UPDATE boat SET nomBateau='$nom',nationnalite='$nationnalite',typeBateau='$type',dateArrive='$dateArrive',dateDepart='$dateDepart',idUser='$idUser' where idBateau='$idBateau' ";
        $query = mysqli_query($con,$sql);
        if ($query) {
            echo "Record updated successfully";
            return true;
        }else {
            echo "Error updating record: " . mysqli_error($con);
            return false;
        }
        mysqli_close($con);

    }
    function Armada_DeleteBoat($idBateau){
        $con = Armada_Connection();
        $sql = "DELETE FROM boat where idBateau='$idBateau'";
        $query = mysqli_query($con,$sql);
        if ($query) {
            echo "Record deleted successfully";
            return true;
        } else {
            echo "Error deleting record: " . mysqli_error($con);
            return false;
        }
        mysqli_close($con);

    }

    function Armada_GetName($idUser){
        $con = Armada_Connection();
        $sql = "SELECT * FROM boat inner join user on boat.idUser=user.id WHERE idUser = '$idUser'";
        $query = mysqli_query($con,$sql);
        $rowCount = mysqli_num_rows($query);
        mysqli_close($con);
        if ($rowCount == 0) {
            return null;
        }
        return mysqli_fetch_array($query);
    }
    //Check whether date is logique
    function Armada_CheckDate($DateArrive,$DateDepart)
    {
        if($DateDepart>$DateArrive){
            return true;
        }
        return false;
        
    }
    //Ensure that two passwords are the same in the page signup.php
    function Armada_CheckPassword($Password,$CheckPassword){
        if($Password===$CheckPassword){
            return true;
        }
        return false;
    }

    function Armada_AfficheUtilisateur()
    {
        $con = Armada_Connection();
        $sql = "SELECT nom,prenom,fonction FROM user inner join statut on user.idStatut=statut.statut_id";
        $query = mysqli_query($con,$sql);
        mysqli_close($con);
        if (mysqli_num_rows($query) > 0) {
            
            while($row = mysqli_fetch_assoc($query)) {
                echo "nom: " . $row["nom"]. " - prenom: " . $row["prenom"]. " fonction:" . $row["fonction"]. "<br>";
            }
        } else {
            echo "null";
        }
    }

    function Armada_ChangeFonction($idUtilisateur,$idStatut)
    {   
        $con = Armada_Connection();
        $sql = "UPDATE user SET idStatut ='$idStatut' where id='$idUtilisateur'";
        $query = mysqli_query($con,$sql);
        if ($query) {
            echo "Record updated successfully";
            return true;
        }else {
            echo "Error updating record: " . mysqli_error($con);
            return false;
        }
        mysqli_close($con);
    }

    function Armada_UpdateImage($image,$idBoat){
        $con = Armada_Connection();
        $sql = "UPDATE boat SET imageBateau ='$image' where idBateau='$idBoat'";
        $query = mysqli_query($con,$sql);
        if ($query) {
            echo "Record updated successfully";
            return true;
        }else {
            echo "Error updating record: " . mysqli_error($con);
            return false;
        }
        mysqli_close($con);
    }
    
    function Armada_UpdatePDF($Caracteristique,$idBoat){
        $con = Armada_Connection();
        $sql = "UPDATE boat SET caracteristique ='$Caracteristique' where idBateau='$idBoat'";
        $query = mysqli_query($con,$sql);
        if ($query) {
            echo "Record updated successfully";
            return true;
        }else {
            echo "Error updating record: " . mysqli_error($con);
            return false;
        }
        mysqli_close($con);
    }


?>
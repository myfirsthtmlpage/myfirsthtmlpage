<?php
include 'fonctions.php';
$idBateau=$_GET['var'];
if(!isset($_COOKIE['ArmadaLogin']))
{
  echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>Gestion du bâteau</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--link href="styles/login.css" rel="stylesheet"-->
    
  </head>

  <body  >
    
    <div class="container">
        <div class ="row">
          <div class="col-sm-6">
            
            <?php $boat=Armada_GetBoat3($idBateau); 
                  $user=Armada_GetName($boat['idUser']);
            ?>
            <form class ="form-horizontal"role="form" method="post" action="checkGestionBateau.php?var=<?php echo $idBateau;?>" enctype="multipart/form-data">
                <div class="form-group">
                     <h1 class="h3 mb-3 font-weight-normal">Gestion du bâteau</h1>
                     <label for="inputName" class="sr-only">Name</label>
                     <input type="text" name="inputName" class="form-control" value="<?php echo $boat['nom']?>" required autofocus>
                     <label for="inputNationnalite" class="sr-only">Nationnality</label>
                     <input type="text" name="inputNationnalite"  class="form-control" value="<?php echo $boat['nationnalite']?>" required>
                     <label for="inputType" class="sr-only">Type</label>
                     <input type="text" name="inputType"  class="form-control" value="<?php echo $boat['typeBateau']?>" required>
                     <label for="inputDateArrive" >Arrive Date</label>
                     <input type="date" name="inputDateArrive"  class="form-control" value="<?php echo $boat['dateArrive']?>" required>
                     <label for="inputDateDepart" >Leave Date</label>
                     <input type="date" name="inputDateDepart"  class="form-control" value="<?php echo $boat['dateDepart']?>" required>
                     <label for="inputFirstName" >First Name of responsable</label>
                     <input type="text" name="inputFirstName"  class="form-control" value="<?php echo $user['prenom']?>" required>
                     <label for="inputLastName" >Last Name of responsable</label>
                     <input type="text" name="inputLastName"  class="form-control" value="<?php echo $user['nom']?>" required>
                     
                </div>

                <button class="btn btn-lg btn-primary " type="submit" name= "modifier">Modifier</button>
                <button class="btn btn-lg btn-primary " type="submit" name= "supprimer">Supprimer</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
            </form>
          </div>
        </div>
    </div>
    <?php
    ?>
  
</body>
</html>

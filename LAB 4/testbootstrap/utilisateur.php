<?php
include_once 'fonctions.php';
if(!isset($_COOKIE['ArmadaLogin']))
{
    echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
    

        <title>Page d'utilisateur</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/utilisateur.css" rel="stylesheet">
        
    
    
    </head>
    <body>
        <div class="jumbotron"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h3>Page d'utilisateur</h3> 
                    <?php
                    $user = Armada_GetUtilisateur($_COOKIE['ArmadaLogin']);
                    $statut=Armada_GetStatut($user['idStatut']);
                    echo "<p> Nom:".$user['nom']."</p>";
                    echo "<p> Préom:".$user['prenom']."</p>";
                    echo "<p> Email:".$user['email']."</p>";
                    echo "<p> Fonction:".$statut['fonction']."</p>";
                    ?>
                </div>
                <?php
                    $con=Armada_Connection();
                    $sql="SELECT * FROM boat";
                    $query=mysqli_query($con,$sql);
                    $ImageArray=array();
                    if(mysqli_num_rows($query) >0){

                        while ($row = mysqli_fetch_array($query)){
                    
                            $ImageArray[] = $row;
                        }
                    }
                ?>
                <div class="col-sm-5">
                        <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                            <ol class="carousel-indicators">
                                <?php 
                                    for($index=0;$index<count($ImageArray);$index++){
                                        if($index==0){
                                            echo ' <li data-target="#carousel" data-slide-to="'.$index.'" class="active"></li>';
                                        }else{
                                            echo ' <li data-target="#carousel" data-slide-to="'.$index.'"></li>';
                                        }
                                    }
                                ?>
                            </ol>
                            <div class="carousel-inner">
                                <?php 
                                    for($index=0;$index<count($ImageArray);$index++){
                                        if($index==0){
                                            echo '<div class="item active">
                                                     <img src="data:image/jpeg;base64,'.base64_encode($ImageArray[$index]['imageBateau']).'"/>
                                                 </div>';
                                        }else{
                                            echo '<div class="item">
                                            <img src="data:image/jpeg;base64,'.base64_encode($ImageArray[$index]['imageBateau']).'"/> 
                                                  </div>';
                                        }
                                    }
                               
                                ?>
                                
                            </div>
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                            </a>
                        </div>                            
                </div>
                <div class="col-sm-4">
                    <a href="boat.php">créer un bateau</a>

<form action="user.php" method="post">
    <input type="submit" name="deconnect" value="Deconnecter" />
</form>

<?php
    if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['deconnect']))
    {
        setcookie('ArmadaLogin',NULL);
        header('Location: index.php');
    }
    
?>
                </div>
            </div>
        </div>










        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        

    </body>
    
    
        

    
    
    
    

   

    
   
</html>
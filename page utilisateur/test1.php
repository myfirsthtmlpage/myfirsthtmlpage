<!DOCTYPE html>
<html>
<head>
   <title>carousel</title>
   <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
   <link href="bootstrap-3.3.7-dist/css/forme_images.css" rel="stylesheet">
</head>
<body>
<div class="jumbotron"></div>
<div class="container">
<div class="profit">
      <h2><strong>profit</strong></h2>
      <p>
            <ul>
                  <li>nom</li>
                  <li>prenom</li>
                  <li>e-mail</li>
                  <li>identifiant</li>
                  <li>rôle</li>
            </ul>
      </p>
</div>
<div class="container coverflow">
   <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>   
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
            <div class="item active">
                  <img src="bootstrap-3.3.7-dist/images/1.jpg" alt="First slide">
            </div>
            <div class="item">
                  <img src="bootstrap-3.3.7-dist/images/2.jpg" alt="Second slide">
            </div>
            <div class="item">
                  <img src="bootstrap-3.3.7-dist/images/3.jpg" alt="Third slide">
            </div>
      </div>
   <!-- Controls -->
      <a class="carousel-control left" href="#myCarousel" 
            data-slide="prev">‹</a>
      <a class="carousel-control right" href="#myCarousel" 
            data-slide="next">›</a>
   </div> 
</div>
<div class="container ">



</div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script>
      $(function(){
            $('.coverflow').css('max-width',$('.coverflow img').width());
      });
</script>
<script>
      $('.carousel').carousel({
            interval:2000
      })
</script>
</body>
</html>
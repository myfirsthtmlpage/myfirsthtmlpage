<?php
include 'fonctions.php';
if(!isset($_COOKIE['ArmadaLogin']))
{
  echo "<script>alert('You need login first');location.href='login.php'</script>";
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>Créer un bâteau</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--link href="styles/login.css" rel="stylesheet"-->
    
  </head>

  <body  >
    
    <div class="container">
        <div class ="row">
          <div class="col-sm-6">
            
    
            <form class ="form-horizontal"role="form" method="post" action="checkboat.php" enctype="multipart/form-data">
                <div class="form-group">
                     <h1 class="h3 mb-3 font-weight-normal">Créer un bâteau</h1>
                     <label for="inputName" class="sr-only">Name</label>
                     <input type="text" name="inputName" class="form-control" placeholder="Nom du bâteau" required autofocus>
                     <label for="inputNationnalite" class="sr-only">Nationnality</label>
                     <input type="text" name="inputNationnalite"  class="form-control" placeholder="Nationnalité" required>
                     <label for="inputType" class="sr-only">Type</label>
                     <input type="text" name="inputType"  class="form-control" placeholder="Type du bâteau" required>
                     <label for="inputDateArrive" >Arrive Date</label>
                     <input type="date" name="inputDateArrive"  class="form-control" placeholder="Date d'arrivé" required>
                     <label for="inputDateDepart" >Leave Date</label>
                     <input type="date" name="inputDateDepart"  class="form-control" placeholder="Date de départ" required>
                     <label for="inputFirstName" class="sr-only">First Name</label>
                     <input type="text" name="inputFirstName"  class="form-control" placeholder="Prénom" required>
                     <label for="inputLastName" class="sr-only">Last Name</label>
                     <input type="text" name="inputLastName"  class="form-control" placeholder="Nom" required>
                     <label for="inputImage" class="sr-only">Image</label>
                     <input type="file" name="inputImage"  class="form-control" placeholder="Image du bâteau" required>
                     <label for="inputCaracteristique">Caractéristique:</label>
                     <textarea class="form-control" name="inputCaracteristique"rows="3"></textarea>
                </div>

                <button class="btn btn-lg btn-primary " type="submit" name= "confirm">Confirmer</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
            </form>
          </div>
        </div>
    </div>
    
    
    
    

   

    
  </body>
</html>